API'S :
users: https://jsonplaceholder.typicode.com/users
posts: https://jsonplaceholder.typicode.com/posts?userId=Y // to get posts for userId = Y
albums: https://jsonplaceholder.typicode.com/albums?userId=Y // to get albums for userId = Y
photos: https://jsonplaceholder.typicode.com/photos?albumId=x // to get photos for albumId = x

Note:
Display information for first 10 users only.
Fetch results for only 1 album for each user.
Display only first 5 photos for an album.

Problem:
Create a react app with DisplayUser component
that takes user information and
displays them in a clean UI.

    Feel free to create smaller components for showing posts, albums information and photos.

    Make sure to display user's name, address ( city, street, zip), phone number, email,
    photo/thumbnail and posts.

    Make sure overall UI look decent on mobile as well. (Mobile first approach)
