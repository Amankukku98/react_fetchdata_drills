import React, { Component } from "react";
import './displayUser.css';
import ShowUser from "../User/showuser";

class DisplayUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: "",
            post_data: ""
        };
    }

    componentDidMount() {
        fetch(
            "https://jsonplaceholder.typicode.com/users")
            .then((res) => res.json())
            .then((user_data) => {
                this.setState({
                    items: user_data

                });
            })
    }
    render() {
        const { items, post_data } = this.state;
        return (
            <ul className="main-container">
                {(items || []).map(eachUser => {
                    return <ShowUser key={eachUser.id} userData={eachUser} />
                })}

            </ul>
        )


    }
}

export default DisplayUser;