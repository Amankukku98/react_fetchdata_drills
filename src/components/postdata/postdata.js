import React, { Component } from "react";


class PostData extends Component {

    constructor(props) {
        console.log("hii");
        super(props);

        this.state = {
            post_data: ""
        };
    }
    componentDidMount() {
        let { id } = this.props;

        fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)
            .then((res) => res.json())
            .then((data) => {
                this.setState({
                    post_data: data,
                })

            })
    }
    render() {
        const { post_data } = this.state;
        console.log(post_data)
        return (
            <ul>
                {(post_data || []).map(eachPost => {
                    return <li>body: {eachPost.body}</li>
                })}

            </ul>
        )


    }
}

export default PostData;