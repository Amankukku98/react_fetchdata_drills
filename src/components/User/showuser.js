import React from 'react';
import './showuser.css'

import PostData from '../postdata/postdata';

const ShowUser = (props) => {
    const { userData } = props

    const { name, phone, email, id } = userData
    const { city, zipcode, street } = userData.address

    return (
        <>
            <div className="container">
                <h1>Name:{name}</h1>
                <h2>Address</h2>
                <ul>
                    <li>City: {city}</li>
                    <li>street: {street}</li>
                    <li>Zipcode: {zipcode}</li>
                </ul>
                <h2>Phone:{phone}</h2>
                <h2>Email:{email}</h2>


                <div className="post">
                    <h1>Post Data:</h1>
                    <PostData id={id} key={id} />

                </div>
            </div>
        </>

    );
}
export default ShowUser;